var gulp = require('gulp'),
sass = require('gulp-sass'),
browsersync = require('browser-sync'),
php = require('gulp-connect-php'),
concat = require('gulp-concat'),
minify = require('gulp-minify');

function style(){
    return (
        gulp.src('scss/app.scss')
        .pipe(sass()).on('error', sass.logError)
        .pipe(gulp.dest('public/css'))
        .pipe(browsersync.stream())
    );
}

function script(){
    return (
        gulp.src([
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
        ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public/js/')) // save .js
        .pipe(minify({
            ext:{
                src:'.js',
                min:'.min.js'
            }
        }))
        .pipe(gulp.dest('./public/js/'))
        .pipe(browsersync.stream())
    );
}

function reload(){
    browsersync.reload();
}

function sync(){
    php.server({}, function (){
        browsersync({
            proxy: '127.0.0.1:8000/public'
        });

        gulp.watch(['./scss/*.scss', './public/*.php' ]).on('change', function () {
            style();
            script();
            browsersync.reload();
        });
    });
}

exports.style = style;
exports.reload = reload;
exports.sync = sync;
exports.script = script;