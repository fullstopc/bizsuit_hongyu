<script src="js/app.js"></script>
<script>
var InitHashPage = function(){
    var hash = window.location.hash;
    if(hash == "#contact"){
        $("#wrapper").addClass("contactpage").removeClass("homepage");
        $(".header-nav .nav-home").removeClass("active");
        $(".header-nav .nav-contact").addClass("active");
    }
    else{
        $("#wrapper").addClass("homepage").removeClass("contactpage");
        $(".header-nav .nav-home").addClass("active");
        $(".header-nav .nav-contact").removeClass("active");
    }
}
$(function(){
    InitHashPage();
    $(window).on('hashchange', function(e) {
        InitHashPage();
    });
});
</script>