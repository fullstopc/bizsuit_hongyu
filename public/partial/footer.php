<footer id="footer">
    <div class='footer-inner container'>
        <div class="footer__logo">
            <img src="img/logo-white.png" class="" />
        </div>
        <div class="footer__socialmedia">
            Find us on social media
            <div class="footer__socialmedia__list">
                <a class="footer__socialmedia__link" target="_blank" href="https://www.facebook.com/hongyuwc/">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a class="footer__socialmedia__link" target="_blank" href="https://m.me/hongyuwc">
                    <i class="fab fa-facebook-messenger"></i>
                </a>
                <a class="footer__socialmedia__link" target="_blank"
                    href="https://api.whatsapp.com/send?phone=60165468140&text=Hi, I contacted you Through your website.">
                    <i class="fab fa-whatsapp"></i>
                </a>
            </div>
        </div>
        <div class="footer__contact">
            <div class="footer__contact__list">
                <a class="footer__contact__item" taget="_blank" href="tel:60165468140">
                    <span class="footer__contact__item__icon">
                        <i class="fas fa-phone"></i>
                    </span>
                    <span class="footer__contact__item__text">+6016 546 8140</span>
                </a>
                <a class="footer__contact__item" tager="_blank" href="mailto:myself73@yahoo.com">
                    <span class="footer__contact__item__icon">
                        <i class="fas fa-envelope"></i>
                    </span>
                    <span class="footer__contact__item__text">myself73@yahoo.com</span>
                </a>
                <a class="footer__contact__item" target="_blank" href="https://www.google.com/maps/place/HongYu+WingChun+Martial+Art/@1.5190061,103.6780697,17z/data=!4m8!1m2!2m1!1s38-02,+Jalan+Pertama,Pusat+Perdagangan+Danga+Utama,81300+Skudai,+Johor,+Malaysia!3m4!1s0x31da73ab702f1891:0xfa49c2bb59028eeb!8m2!3d1.5193889!4d103.6803861">
                    <span class="footer__contact__item__icon">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                    </span>
                    <span class="footer__contact__item__text">
                        38-02, Jalan Pertama, <br />
                        Pusat Perdagangan Danga Utama,<br />
                        81300 Skudai, Johor, Malaysia
                    </span>
                </a>
            </div>
        </div>
    </div>
</footer>