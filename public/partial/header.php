<header id="header">
    <div class="header-inner container">
        <div class="header-right">
            <div class="header-nav">
                <ul class="nav-menu nav-one">
                    <li class="nav-item active"><a class="nav-link nav-home" href="#">HOME</a></li>
                    <li class="nav-splitter"></li>
                    <li class="nav-item"><a class="nav-link nav-contact" href="#contact">CONTACT</a></li>   
                </ul>
            </div>
        </div>
    </div>
</header>