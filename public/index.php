<html>

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MRW54NK');</script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125713953-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-125713953-5');
    </script>
    
    <title>HongYu::Wing Chun Class | 鸿羽 咏春拳【潘氏】:: 柔佛新山士古来武术课程</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="og:title" content="Hong Yu" />
    <meta property="og:type" content="Website" />
    <meta property="og:url" content="//hongyuwc.com" />
    <meta property="og:image" content="//www.http://hongyuwc.com/img/logo.png" />
    <meta property="og:site_name" content="【五福城】新山士古来咏春拳武术课程 - 修身养性 | Hongyu provides wing chun martial art class. Improves mind, body, and soul. Locate us at Danga Utama, Skudai" />
    <meta property="og:description" content="【五福城】新山士古来咏春拳武术课程 - 修身养性 | Hongyu provides wing chun martial art class. Improves mind, body, and soul. Locate us at Danga Utama, Skudai" />
    <meta name='keywords' content='wing chun jb, wing chun skudai, wing chun class, martial art jb, 咏春教学, 新山咏春, 新山武术, 五福城武术' />
    <meta name='description' content='【五福城】新山士古来咏春拳武术课程 - 修身养性 | Hongyu provides wing chun martial art class. Improves mind, body, and soul. Locate us at Danga Utama, Skudai' />
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MRW54NK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="wrapper" class="homepage">
        <?php include_once 'partial/header.php'; ?>
        <section id="content">
            <div class="content__inner container">
                <div class="content__page content--homepage">
                    <div class="content__box" style="">
                        <div class='content__logo'></div>
                        <br />
                        <div class="content__yongchun"></div>
                        <a href='#contact' class='content__button'></a>
                    </div>

                </div>
                <div class="content__page content--contactpage">
                    <div class="content__box">
                        <div class="content__title">

                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-7">
                                    <div><b>Our Class time is list on below:</b></div>
                                    <div class="grid-style">
                                    <div>Monday :</div><div> 8.30PM</div>
                                    <div>Friday :</div><div> 9.00PM</div>
                                    <div>Saturday :</div><div> 8.30PM</div>
                                    </div>
                                    <br />
                                    <div><b>Training Fee:</b></div>
                                    <div>Below 12 years old : RM 50 / Per Month</div>
                                    <div>Above 12 years old : RM 90 / Per Month</div>
                                    <br />
                                    <div>
                                        <b>Registration Fee : </b> RM 15
                                    </div>
                                    <br />
                                </div>
                                <div class="col-12 col-sm-12 col-md-5">
                                    <div><b>咏春上课时间是:</b></div>
                                    <div>星期一 : 8.30PM</div>
                                    <div>星期五 : 9.00PM</div>
                                    <div>星期六 : 8.30PM</div>
                                    <br />
                                    <div><b>学费:</b></div>
                                    <div>12岁或以下 : RM 50 / 月</div>
                                    <div>13岁或以上 : RM 90 / 月</div>
                                    <br />
                                    <div>
                                        <b>报名费 : </b> RM 15
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container p-0">
                        <div>
                            <div>
                                <span>*</span> Please feel free to contact us if any enquiry. Walk in to
                                register during class time also available, Thanks.
                            </div>
                            <div>
                                <span>*</span> 任何详情可以联系我们，也可以在上课时间来报名或是询问，谢谢。
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include_once 'partial/footer.php'; ?>
    </div>
    <?php include_once 'partial/script.php'; ?>
</body>

</html>