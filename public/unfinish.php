<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name='keywords' content='' />

    <title>Cupid Jewellery</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
<div id="wrapper">
    <?php include_once 'partial/header.php'; ?>
    <section id="content">
        <h1 class="text-center h-100 d-flex align-items-center justify-content-center">The page is under construction...</h1>
    </section>
    <?php include_once 'partial/footer.php'; ?>
</div>
<?php include_once 'partial/script.php'; ?>
</body>
</html>